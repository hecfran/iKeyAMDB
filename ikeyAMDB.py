#############################################################################
# The following library is provides an API to annotate files using a mongodb library
# It enables: 
# * to calculate inter annotator agreement 
# * amend annotations 
# * active learning 
# * reproduce and old dataset
#############################################################################


__version__ = '0.1'
__author__ = 'Hector Franco'
# style guide: https://www.python.org/dev/peps/pep-0008/



import pymongo
from pprint import pprint

#temp_output_db= 'tempTables'

from pymongo import MongoClient
####import gridfs
####db = MongoClient().gridfs_example
#####fs = gridfs.GridFS(db)

from PIL import Image
from io import BytesIO
import hashlib
import os

SaveFolder = "./DBFiles/"
import os
if not os.path.exists(SaveFolder):
    os.makedirs(SaveFolder)
from typing import Union, List, NewType

UserId = NewType('UserId', str) # used for filed author. 
LabelId = NewType('LabelId',str) # used to specifiy the field that is annotated. 
# add more typed labels. 


def generateMultiClassDataset(db, listFeaturesInvolved, outputFolder, featureSplit, endingFilteringSteps=[],filteringAnnotation=[]):
    # this function creates a data set ready for machine learning
    # note this function should be placed at the client side, not at the server
    # @todo : re-structure the system. 
    #         produce a file summary of the created dataset

    documents = GetSimplifiedTable(db, listFeaturesInvolved,removeUnlabelled=True, filteringAnnotation=filteringAnnotation, endingFilteringSteps=endingFilteringSteps)
    if os.path.isdir(outputFolder):
        raise Exception("output folder exist, please select a different output folder or delete the current one")
    else:
        os.makedirs(outputFolder)
    for doc in documents:
        subfolderLabel = doc[featureSplit]
        subfolder = os.path.join(outputFolder,subfolderLabel)
        if not os.path.exists(subfolder):
            os.makedirs(subfolder)
        fileid = doc['_id']
        deployFile(fileid,subfolder,db)



#def generateDataset(listFeaturesInvolved, outputFolder, featureSplit, filteringSteps=[],generationDate=None):
#    documents = GetSimplifiedTable(db, listlistFeaturesInvolvedFeatures,removeUnlabelled=True, filteringSteps=filteringSteps)
#    print("this function is not implemented")
#    raise NotImplementedError


def deployFile(fileid, outputFolder,db):
    contents = getFileContentHD(fileid)
    ext = getExtension(fileid,db)
    pat = os.path.join(outputFolder,fileid)+ext
    f = open(pat, 'wb')
    f.write(contents)
    f.close()


def getFileContentHD(id: str):
    # opens a file and returns its content as a stream of bytes. 
    with open(SaveFolder+id, 'rb') as f:
        return f.read()

def insetFileStreamHD( contents, author:UserId ,db) -> str:
    # inserts a file in the DB and saves the author that uploaded the file
    # returns the id of the inserted file. 
    # ignore inserting duiplicate files, but save the author that try to re-save the file.
    fileid = getHash(contents)
    path = SaveFolder+fileid
    if (not os.path.isfile(path)):
        f = open(path, 'wb')
        f.write(contents)
        f.close()


    coll = getAtributeTable(db,"insertedFiles")
    insertAtribute(coll,  "" , author, fileid) # inserts a line with date (_id) and author
    return fileid

    ## for a given file strem it inserts it in the db and returns the file ID



def getExtension(fileID:str,database)->str:
    # returns the extension of the file fetched from the DB. 
    steps=[
        {"$match":{"fileID":fileID}},
        {"$limit":1}
    ]
    fileExtensionCollection = getAtributeTable(database, "file_extension")
    agg_result = fileExtensionCollection.aggregate(steps)
    for i in agg_result:
        return i['information']
    return ""
        


def getHash(contentFile)->str: 
    # uses the conetent of a file to generate a hash value
    h = hashlib.sha256() # Construct a hash object using our selected hashing algorithm
    h.update(contentFile) # Update the hash using a bytes object

    return h.hexdigest()


def fileExists(hashtag:str) -> bool:
    # checks if file is already stored in the DB
    return os.path.isfile(SaveFolder+hashtag)


def insertFileWithLabelDrive(labelName:LabelId, labelValue, file:str , author:str,db,  confidence = -1) -> str:
    # insert file in the DB and attaches a label, this is used to add labels to files by re-uploading the files.     
    # labelName: string that specify the label name that is annotated for.
    # labelValue: the value of the annotation, can be a string or a json.
    # file: path to the file that will be inserted
    # author: id of the user inserting the file in the DB. 
    # db: client mongodb object.
    # confidence: the confidence of the author that the label is correct. (this is used for active learning)


    #Open the image in read-only format.
    if (not os.path.isfile(file)):
        raise Exception(file+ " file not found")


    with open(file, 'rb') as f:
        contents = f.read()
    file_extension = ""
    _filename, file_extension = os.path.splitext(file)

    return insertFileWithLabelStream(labelName, labelValue, contents , author,db, file_extension, confidence = confidence)


def insertFileWithLabelStream(labelName:LabelId, labelValue, contents , author,db, file_extension="", confidence = -1):
    # inserts file from a stram of data into the db. 
    # labelName: string that specify the label name that is annotated for.
    # labelValue: the value of the annotation, can be a string or a json.
    # contents: stram of bytes with the content of the file 
    # author: id of the user inserting the file in the DB. 
    # db: client mongodb object.
    # file_extension: the extension of the original file
    # confidence: the confidence of the author that the label is correct. (this is used for active learning)

    fileID = insetFileStreamHD( contents, author,db)
    mycol = db["originalFiles"]   
    element = {
        "_id":  fileID
    }
    
    if mycol.find(element).count() == 0:
        x = mycol.insert_one(element)

    collection = getAtributeTable(db, labelName)
    insertAtribute(collection, labelValue, author, fileID, confidence = confidence)
    collectionATR = getAtributeTable(db, "file_extension")
    insertAtribute(collectionATR, file_extension, author, fileID)
    return fileID



def insertFileDrive(file:str , author,db):
    
    #Open the file/image in read-only format.
    with open(file, 'rb') as f:
        contents = f.read()

    _filename, file_extension = os.path.splitext(file)
    collectionATR = getAtributeTable(db, "file_extension")
    
    fileID = insetFileStreamHD( contents, author,db)

    insertAtribute(collectionATR, file_extension, author, fileID)
    return fileID 




def insetFileStream(contents, author,db):
    fileid = getHash(contents)


    if (fileExists(fileid)==False):
        fileid2 = insetFileStreamHD( contents, author,db)
        assert fileid == fileid2
        
    coll = getAtributeTable(db,"insertedFiles")
    insertAtribute(coll,  "" , author, fileid) # inserts a line with date (_id) and author
    return fileid

    ## for a given file strem it inserts it in the db and returns the file ID


def getAtributeTable(database, atributeName:LabelId):
    # returns a mongodb collection
    mycol = database[atributeName]   
    return  mycol


def insertAtribute(atributeTable, information, author:UserId, fileId:str, confidence = -1):
    element = {
        "information": information, 
        "author" : author, 
        "fileID" : fileId, 
        "confidence" : confidence 
    }
    x = atributeTable.insert_one(element)
    return x 

scoreDefault = "$_id" # default order for finding latest annotation.

def generateLookupLine(atributeTable:LabelId, sortEx=scoreDefault,removeUnlabelled=False, simplifyLabel=True,filteringAnnotation=[]):
    # generates step to fetch atribute from table. 

    result = [{
        '$lookup': 
                    {'from' : atributeTable,
                     'let':{'file_id':"$_id"},
                     'pipeline': generatePipeLineForLookup(sortEx, filteringAnnotation=filteringAnnotation),                     
                     'as' : atributeTable
                    }
            }]
    if removeUnlabelled:
        result.append({
            "$match":{
                 "$expr":{ "$gte": [{"$size":"$"+atributeTable},1] }     # greater or equal to 1 element, otherwise remove. 
            }
        })
    if simplifyLabel:
        result.append({
            #"$set":{ atributeTable: "$"+atributeTable+".information"}
            "$set":{ atributeTable: {"$first": "$"+atributeTable+".information"}}
             
        })        
    return result

def generatePipeLineForLookup( sortEx, filteringAnnotation=[]): # ausmes fileID as set variable
    #sortEx specifies how to sort the annotations to identify the dominant one in case of multiple annotations over the same label. 
    result = filteringAnnotation 
    result = result+ [ 
                {
                    '$match':{  "$expr":{ "$eq": ["$$file_id","$fileID"] }     } # for each file_id
                },
                {
                    "$set":{                        
                        "sortValue" : sortEx
                    }
                },                
                {'$sort': { "sortValue": -1}}, # sort cronologically.                 
                { "$limit":1},
                #{'$set':{
                #        "addedvalue":'77',
                #        "head_UIDA":"$$uida"                         
                #     }}
               ]
    return result
    #returns pipeline


def GetSimplifiedTable(db, listFeatures,removeUnlabelled=False, endingFilteringSteps=[],filteringAnnotation=[],simplifyLabel=True):
    originalFiles=db['originalFiles'] 
    steps=[]
    for f in listFeatures:
        s = generateLookupLine(f,simplifyLabel=simplifyLabel,removeUnlabelled=removeUnlabelled,filteringAnnotation=filteringAnnotation)    
        steps = steps+s
    steps=steps+endingFilteringSteps

    agg_result= originalFiles.aggregate(steps)

    return agg_result
