import pymongo
import ikeyAMDB as ik

import unittest
import pprint
import os
from pymongo import MongoClient 


mongoServer = "localhost"
mongoServer = "mongo"
class insertFileTests(unittest.TestCase):

    def test_insetFileStreamHD(self):
        # inserts images from the hard drive
        print("test_insetFileStreamHD")
        file = "./test/Ireland_wiki.jpg"
        with open(file, 'rb') as f:
            contents = f.read()
        myclient = pymongo.MongoClient("mongodb://"+mongoServer+":27017/")
        mydb = myclient["TUTORIAL"]
        
        a = ik.insetFileStreamHD(contents, 'author', mydb)
        print(a)

        b = ik.getFileContentHD(a)


    def test_getHash(self):
        # gets hash id from file
        print("test_getHash")
        file = "./test/Ireland_wiki.jpg"
        with open(file, 'rb') as f:
            contents = f.read()
        a = ik.getHash(contents)
        print(a)

    def test_insertFileTest(self):
        #inserts file along with a label. 
        print("test_insertFileTest")
        from pymongo import MongoClient
        #import gridfs  # mongodb file sistem disabled. 
        #dbFiles = MongoClient().gridfs_example
        #fileSystemMGDB = gridfs.GridFS(dbFiles)
        file = "./test/Ireland_wiki.jpg"

        myclient = pymongo.MongoClient("mongodb://"+mongoServer+":27017/")
        mydb = myclient["TUTORIAL"]

        a = ik.insertFileDrive(file , author="author",db=mydb)
        print(a)
        print("compleated")


    def test_insertFileTestEXTRACT(self):
        # extracts file from the DB ater inserting it. 
        print("test_insertFileTestEXTRACT")
        from pymongo import MongoClient
        #import gridfs
        #dbFiles = MongoClient().gridfs_example
        #fileSystemMGDB = gridfs.GridFS(dbFiles)
        file = "./test/Ireland_wiki.jpg"

        myclient = pymongo.MongoClient("mongodb://"+mongoServer+":27017/")
        mydb = myclient["TUTORIAL"]

        a = ik.insertFileDrive(file , author="author",db=mydb)
        print(a)
        #ik.writeFileToDrive("C:/Users/hecfr/Desktop/",a,fileSystemMGDB,".jpg")  



        b = ik.getExtension(a,mydb)
        print(b)


    def test_insertFileLabel(self):
        # adds label to a file in the db
        print("test_insertFileLabel")
        from pymongo import MongoClient
        #import gridfs
        #db = MongoClient().gridfs_example
        #fileSystemMGDB = gridfs.GridFS(db)
        file = "./test/Ireland_wiki.jpg"

        myclient = pymongo.MongoClient("mongodb://"+mongoServer+":27017/")
        mydb = myclient["TUTORIAL"]
        a = ik.insertFileWithLabelDrive("RL", "R", file , "me",mydb, confidence = -1)
        #        (fileSystemMGDB, file , author="author",db=mydb)
        print(a)

        # checks that the system fails adding label to an unexisting file. 
        exits = ik.fileExists("aaaaaaaaaaaaaa")
        assert exits==False, "un-existing file present"
        print(exits)        


        exits = ik.fileExists(a)
        assert exits, "files not being saved"
        print(exits)
        
        
        
        print("compleated")
        


class generateLookupLineTests(unittest.TestCase):
    def test_generateLookupLine(self):
        print("cheking test generateLookupLine")
        steps = ik.generateLookupLine("EUR_DOLLAR")
        pprint.pprint(steps)

    def test_auxiliar(self):
        print("test_auxiliar")

        client=MongoClient() 
        # Connect with the portnumber and host 
        client = MongoClient("mongodb://"+mongoServer+":27017/") 
        # Access database 
        mydatabase = client['database'] 
        # Access collection of the database 
        originalFiles=mydatabase['originalFiles'] 
        steps = [
            {'$lookup': {'as': 'EUR_DOLLAR',
              'from': 'EUR_DOLLAR',
              'let': {'file_id': '$_id'},
              'pipeline':[
                  {'$match': {'$expr': {'$eq': ['$$file_id', '$fileID']}}}

              ]
              }}]
   
        pprint.pprint(steps)    
        
        agg_result= originalFiles.aggregate(steps)
        for i in agg_result:
            pprint.pprint(i)    

    def test_GetSimplifiedTable(self):
        print("test_GetSimplifiedTable")
        client=MongoClient() 
        # Connect with the portnumber and host 
        client = MongoClient("mongodb://"+mongoServer+":27017/") 
        # Access database 
        db = client['database'] 
        listFeatures=['EUR_DOLLAR','RL']

        agg_result = ik.GetSimplifiedTable(db, listFeatures,removeUnlabelled=False)
        

        for i in agg_result:
            pprint.pprint(i)



    def test_lookup(self):
        print("test_lookup")
        client=MongoClient() 
        # Connect with the portnumber and host 
        client = MongoClient("mongodb://"+mongoServer+":27017/") 
        # Access database 
        mydatabase = client['database'] 
        # Access collection of the database 
        originalFiles=mydatabase['originalFiles'] 
        steps = ik.generateLookupLine("EUR_DOLLAR",simplifyLabel=True,removeUnlabelled=True)
        pprint.pprint(steps)

        agg_result= originalFiles.aggregate(steps)
        for i in agg_result:
            pprint.pprint(i)

        steps = ik.generateLookupLine("EUR_DOLLAR",simplifyLabel=False,removeUnlabelled=True)
        pprint.pprint(steps)

        agg_result= originalFiles.aggregate(steps)
        for i in agg_result:
            pprint.pprint(i)

        steps = ik.generateLookupLine("EUR_DOLLAR",simplifyLabel=True,removeUnlabelled=False)
        pprint.pprint(steps)

        agg_result= originalFiles.aggregate(steps)
        for i in agg_result:
            pprint.pprint(i)

        steps = ik.generateLookupLine("EUR_DOLLAR",simplifyLabel=False,removeUnlabelled=False)
        pprint.pprint(steps)

        agg_result= originalFiles.aggregate(steps)
        for i in agg_result:
            pprint.pprint(i)


    
class TestStringMethods(unittest.TestCase):

    
    def test_upper(self):
        self.assertEqual('foo'.upper(), 'FOO')

    def test_isupper(self):
        self.assertTrue('FOO'.isupper())
        self.assertFalse('Foo'.isupper())

    def test_split(self):
        s = 'hello world'
        self.assertEqual(s.split(), ['hello', 'world'])
        # check that s.split fails when the separator is not a string
        with self.assertRaises(TypeError):
            s.split(2)

class TestingCreatedTables(unittest.TestCase):
    def createDirectory(self):
            os.makedirs("./NEWDIRECTORY/")    



    def testInsertImageTwice(self):
        from pymongo import MongoClient
        #import gridfs
        #dbFiles = MongoClient().gridfs_example
        #fileSystemMGDB = gridfs.GridFS(dbFiles)

        myclient = pymongo.MongoClient("mongodb://"+mongoServer+":27017/")
        mydb = myclient["TUTORIAL"]

        #a = ik.insertFileDrive(file , author="author",db=mydb)
        file = "./test/Ireland_wiki.jpg"
        a = ik.insertFileWithLabelDrive("RL", "R", file , "author",mydb,  confidence = 0.4)
        b = ik.insertFileWithLabelDrive("AGE", "33", file , "author",mydb,  confidence = 0.4)

        file = "./test/Map_of_Ireland.png"
        d = ik.insertFileWithLabelDrive("RL", "L", file , "author",mydb,  confidence = 0.4)
        e = ik.insertFileWithLabelDrive("AGE", "21", file , "author",mydb,  confidence = 0.4)
        f = ik.insertFileWithLabelDrive("AGE", "22", file , "badauthor",mydb,  confidence = 0.4)


        c = ik.GetSimplifiedTable(mydb, ["RL","AGE","file_extension"],removeUnlabelled=False)
        for document in c:
            print(document)
        print("..........")

        Filter1 = [{"$not" : {"$match": { "author": "badauthor" } }}]
        Filter1 = [{"author" : {"$ne": "badauthor" }}]
        Filter1 = [{"$match":{"author" : {"$ne": "badauthor" }}}]


        c = ik.GetSimplifiedTable(mydb, ["RL","AGE","file_extension"],removeUnlabelled=False,filteringAnnotation=Filter1)
        for document in c:
            print(document)
            # checks table 



        c = ik.GetSimplifiedTable(mydb, ["RL","AGE","file_extension"],removeUnlabelled=False,filteringAnnotation=Filter1,simplifyLabel=False)
        for document in c:
            pprint.pprint(document)            
        print("compleated")
        print("..........")

        #def generateMultiClassDataset(db, listFeaturesInvolved, outputFolder, featureSplit, filteringSteps=[],generationDate=None):
        ik.generateMultiClassDataset(mydb,["RL"],"./TESTOUTPUTDATASET","RL")







if __name__ == '__main__':
    unittest.main(verbosity=2)